<?php
declare ( strict_types = 1 )
;

namespace UntilDistributed;

use SplQueue;

/**
 * 事务传播id
 *
 * @author mg
 *
 */
class TxId {
	/**
	 * 事务xid
	 *
	 * @var SplQueue
	 */
	private $splQueue;
	
	/**
	 *
	 * @var string
	 */
	// private $xId = '';
	
	/**
	 *
	 * @var TxId
	 */
	private static $obj;
	
	/**
	 * 构造方法
	 */
	private function __construct() {
		$this->splQueue = new SplQueue ();
		
		// $this->xId = $xId;
	}
	public static function getInstance(): self {
		if (! static::$obj instanceof self) {
			static::$obj = new static ();
		}
		
		return static::$obj;
	}
	
	/**
	 * 入列
	 *
	 * @param string $xId
	 */
	public function push(string $xId, int $length = 1): void {
		$i = 0;
		
		while ( $i < $length ) {
			static::$obj->splQueue->enqueue ( $xId );
			$i ++;
		}
	}
	public function pop(): string {
		if (static::$obj->splQueue->isEmpty ()) {
			return '';
		}
		return static::$obj->splQueue->dequeue ();
	}
	
	public function clear(): void {
		while (static::$obj->splQueue->valid()) {
			static::$obj->splQueue->dequeue();
			static::$obj->splQueue->next();
		}
	}
}