<?php

declare ( strict_types = 1 )
;

namespace UntilDistributed;

/**
 * 配置获取
 * 需定义 BASE_PATH 为项目根目录
 */
class ConfigHelper {
	/**
	 * @var array
	 */
	private static $config = [ ];
	/**
	 * 获取公共配置
	 * 需配置常量属性BASE_PATH为根目录
	 * @param string $path
	 * @return array
	 */
	public static function get(string $path = ''): array {
		// 系统的配置
		if (0 === count(static::$config)) {
			static::$config = include BASE_PATH. '/trans.php';
		}
		
		$pathArr = explode ( '.', $path );
		if (count ( $pathArr ) == 0) {
			return static::$config;
		}
		
		$ret = [ ];
		
		foreach ( $pathArr as $key ) {
			$ret = isset ( static::$config [$key] ) ? static::$config [$key] : [ ];
		}
		
		return $ret;
	}
	
	/**
	 * 销毁数据
	 */
	public static function destory(): void {
		static::$config = [ ];
	}
}