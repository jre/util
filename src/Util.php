<?php

declare ( strict_types = 1 )
	;

namespace UntilDistributed;

/**
 * Util
 */
class Util {
	/**
	 * 生成事务组唯一标识
	 * 
	 * @return string
	 */
	public static function createXid(): string {
		return "xid_" . uniqid () . "_" . rand ( 10000, 99999 );
	}
	public static function createTaskKey(): string {
		return "taskkey_" . uniqid () . "_" . rand ( 10000, 99999 );
	}
	
	/**
	 * 生成事务发起者唯一标识
	 * 
	 * @return string
	 */
	public static function createStarterId(string $xid): string {
		return $xid . '_' . 'starter' . '_' . uniqid ();
	}
	
	/**
	 * 生成事务参与者唯一标识
	 * 
	 * @param string $xid
	 *        	[description]
	 * @return [type] [description]
	 */
	public static function createActorId(string $xid): string {
		return $xid . '_' . 'actor' . '_' . uniqid ();
	}
} 